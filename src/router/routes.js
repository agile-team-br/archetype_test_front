
const routes = [
  {
    path: '/',
    component: () => import('layouts/menu'),
    children: [
      { path: '', component: () => import('pages/index') },
      { path: '/responsaveis', component: () => import('pages/responsavel') },
      { path: '/processos', component: () => import('pages/processo') }
    ]
  }
]

export default routes
