# Frontend Gestão de Processos Judiciais

Api REST de integração de processos judiciais.

_Prerequisitos:_
* docker version 1.10.3
* Node Js


## Instalação

1. Para rodar separadamente sigua as instruções:

   `docker build -t <nome_da_imagem> .`
   
2. Rodando a imagem:

   `docker run -p 8083:8080 <nome_da_imagem>`


## Features

- `Vuejs` 
- `Quasar` 

